const express = require('express');
const router = express.Router();

const {ChartJSNodeCanvas} = require('chartjs-node-canvas');
const height = 600;
const width = 1500;
const chartCallback = (ChartJS) => {
    console.log('chart built');
};

const canvasRenderService = new ChartJSNodeCanvas({width, height, chartCallback});

const fs = require('fs');
const readline = require('readline');

let data = [];
let configuration = {};


/* GET home page. */
router.get('/', async function (req, res, next) {
    let dataSets = [];
    let formattedData = {};
    let bloodGlucoseLevelsArr = [];
    let datesArr = []

    const stream = await fs.createReadStream('../mullinshealth/blood_glucose_log.csv');
    const rl = await readline.createInterface({input: stream});

    await rl.on('line', (row) => {
        data.push(row.split(','));
    });

    await rl.on('close', async () => {
        for (let i = 1; i < data.length; i++) {
            bloodGlucoseLevelsArr.push(parseInt(data[i][2].replace(' mg/dl', '')));

            let labelAppend = data[i][3] === 'Fasting' ? ' (fasting)' : ' (2 hours after eating)';
            datesArr.push(data[i][0] + labelAppend);
        }

        formattedData['labels'] = [];
        formattedData['labels'] = [...datesArr];

        dataSets.push({
            label: 'Blood Glucose Levels',
            data: bloodGlucoseLevelsArr,
            fill: false,
            borderColor: 'rgba(8, 111, 187, 1)',
            backgroundColor: 'rgba(8, 111, 187, 1)',
        });

        formattedData['datasets'] = dataSets;

        configuration = {
            type: 'line',
            data: formattedData,
        };

        res.render('health', {
            title: 'Health',
            bloodGlucoseChart: await canvasRenderService.renderToDataURL(configuration)
        });
    });
});

module.exports = router;
