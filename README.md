# MullinsHealth

<!-- TABLE OF CONTENTS -->
<h2 id="table-of-contents">Table of Contents</h2>

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project"> ➤ About The Project</a></li>
    <li><a href="#installation"> ➤ Installation</a></li>
    <li><a href="#about-the-author"> ➤ About the Author</a></li>
    <li><a href="#project-status"> ➤ Project Status</a></li>
  </ol>
</details>

<h2 id="about-the-project"> About The Project</h2>
In the United States there are more than 37 million people with diabetes. I am one of them. I am also a software 
developer, so I wanted an easy way to view my blood glucose numbers.  I was tracking the data in a csv file, but I also 
wanted an easier way to see the trends. I knew there were many chart libraries in JavaScript, so I decided to use 
<a href="https://www.chartjs.org/docs/latest/">ChartJS</a>. I ended up using <a href="https://github.com/SeanSobey/ChartjsNodeCanvas">chartjs-node-canvas</a>
in order to return an image of the chart to the front end. 

<h2 id="installation"> Installation</h2>
<ol>
  <li>Fork this repo and clone to your local dev environment</li>
  <li><code>cd mullinshealth</code> cd into this project</li>
  <li>Run <code>npm install</code></li>
  <li>Run <code>DEBUG=mullinshealth:* npm start</code> to start the server</li>
  <li>Open <a href="http://localhost:3000/health">http://localhost:3000/health</a> to view the "health" page with the blood glucose levels chart</li>
  <li>Create a csv file named "blood_glucose_log.csv" and insert your own data. Data in the included file is just sample data that you can use as reference on how to format the file.</li>
</ol>

<h2 id="about-the-author"> About the Author</h2>
My name is Nick Mullins. I am a director of engineering for a large marketing company. I have been in the industry for 
10+ years. I consider myself a giant nerd. I also play and collect video games

<h2 id="project-status"> Project status</h2>
This is just the start of the project. The only portion I have worked on so far is the chart. Eventually I will add 
more content and style the entire app. I will most likely add additional charts for other data points, such as blood
pressure, weight, etc.

<p>
    <img src="images/blood_glucose_chart.png" alt="Screenshot of the blood glucose chart" width="70%" height="70%" style="display: block; margin: 0 auto">
</p>